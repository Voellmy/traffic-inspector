FROM mitmproxy/mitmproxy:4.0.4

RUN apk add bash

EXPOSE 8080
EXPOSE 8081

COPY run.sh /run.sh

CMD /run.sh