#!/usr/bin/env bash

if [ -z $TARGET ]; then
    echo "No TARGET specified. Set the TARGET env variable to http://[host]:[port]"

    exit 1
fi

if [ -z $PEM_PATH ]; then
    if [ -z $CRT_PATH ] && [ -z $KEY_PATH ]; then
        echo "TLS disabled. Running only on HTTP. To run with a certificate set the CRT_PATH and KEY_PATH variables or the PEM_PATH variable"

        mitmweb --web-iface 0.0.0.0 --mode transparent --mode reverse:${TARGET}
    else
        echo "Using key $KEY_PATH and crt $CRT_PATH"
        cat $KEY_PATH $CRT_PATH > /tmp/cert.pem

        mitmweb --cert /tmp/cert.pem --web-iface 0.0.0.0 --mode transparent --mode reverse:${TARGET}
    fi

else
    echo "Using pem $PEM_PATH"

    mitmweb --cert $PEM_PATH --web-iface 0.0.0.0 --mode transparent --mode reverse:${TARGET}
fi
